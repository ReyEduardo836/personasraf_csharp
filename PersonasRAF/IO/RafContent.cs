﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PersonasRAF.IO
{
    class RafContent
    {
        private string file;
        private int SIZE;

        public RafContent(string file, int size)
        {
            this.file = file;
            this.SIZE = size;
        }

        private Stream headStream
        {
            get => File.Open($"{file}.hd", FileMode.OpenOrCreate, FileAccess.ReadWrite);
        }

        private Stream dataStream
        {
            get => File.Open($"{file}.dat", FileMode.OpenOrCreate, FileAccess.ReadWrite);
        }

        public void create<T>(T t)
        {
            int n, k;
            using (BinaryWriter hWriter = new BinaryWriter(headStream), dWriter = new BinaryWriter(dataStream))
            {
                if (hWriter.BaseStream.Length == 0)
                {
                    hWriter.BaseStream.Position = 0;
                    hWriter.Write(0);
                    hWriter.Write(0);
                }
                using (BinaryReader hReader = new BinaryReader(hWriter.BaseStream), dReader = new BinaryReader(dWriter.BaseStream))
                {
                    hReader.BaseStream.Position = 0;
                    n = hReader.ReadInt32();
                    k = hReader.ReadInt32();


                    long pos = SIZE * k; //new k BUENO 
                    dWriter.BaseStream.Position = pos;

                    PropertyInfo[] info = t.GetType().GetProperties();
                    foreach (PropertyInfo pinfo in info)
                    {
                        Type type = pinfo.PropertyType;
                        Object value = pinfo.GetValue(t);

                        if (type.IsGenericType)
                        {
                            continue;
                        }
                        if (pinfo.Name.Equals("Id", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dWriter.Write(++k);
                            continue;
                        }
                        if (type == typeof(int))
                        {
                            dWriter.Write((int)value);
                        }
                        else if (type == typeof(long))
                        {
                            dWriter.Write((long)value);
                        }
                        else if (type == typeof(float))
                        {
                            dWriter.Write((float)value);
                        }
                        else if (type == typeof(double))
                        {
                            dWriter.Write((double)value);
                        }
                        else if (type == typeof(decimal))
                        {
                            dWriter.Write((decimal)value);
                        }
                        else if (type == typeof(string))
                        {
                            dWriter.Write((string)value);
                        }
                        else if (type == typeof(char))
                        {
                            dWriter.Write((char)value);
                        }
                        else if (type == typeof(bool))
                        {
                            dWriter.Write((bool)value);
                        }
                    }

                    long posh = 8 + (--k * 4); // EDITADO NEW
                    hWriter.BaseStream.Position = posh;
                    hWriter.Write(++k);

                    hWriter.BaseStream.Position = 0;
                    hWriter.Write(++n);
                    hWriter.Write(k);
                }
            }
        }

        public T getById<T>(int id)
        {
            T newValue = (T)Activator.CreateInstance(typeof(T));
            using (BinaryReader hReader = new BinaryReader(headStream), dReader = new BinaryReader(dataStream))
            {
                int n = hReader.ReadInt32();
                int k = hReader.ReadInt32();

                if (id <= 0 || id > k)
                {
                    return default(T);
                }

                long posh = 8 + ((id - 1) * 4);
                hReader.BaseStream.Position = posh;
                int index = hReader.ReadInt32();
                if (index != id) // si el id no es igual que el id en el archivo head, da error
                {
                    return default(T);
                }

                long posd = (id - 1) * SIZE;
                dReader.BaseStream.Position = posd;

                PropertyInfo[] info = newValue.GetType().GetProperties();

                foreach (PropertyInfo pinfo in info)
                {
                    Type type = pinfo.PropertyType;

                    if (type == typeof(int))
                    {
                        pinfo.SetValue(newValue, (int)dReader.ReadInt32());
                    }
                    else if (type == typeof(long))
                    {
                        pinfo.SetValue(newValue, (long)dReader.ReadInt64());
                    }
                    else if (type == typeof(float))
                    {
                        pinfo.SetValue(newValue, (float)dReader.ReadSingle());
                    }
                    else if (type == typeof(double))
                    {
                        pinfo.SetValue(newValue, (double)dReader.ReadDouble());
                    }
                    else if (type == typeof(decimal))
                    {
                        pinfo.SetValue(newValue, (decimal)dReader.ReadDecimal());
                    }
                    else if (type == typeof(char))
                    {
                        pinfo.SetValue(newValue, (char)dReader.ReadChar());
                    }
                    else if (type == typeof(string))
                    {
                        pinfo.SetValue(newValue, (string)dReader.ReadString());
                    }
                    else if (type == typeof(bool))
                    {
                        pinfo.SetValue(newValue, (bool)dReader.ReadBoolean());
                    }

                }
            }
            return newValue;
        }

        public T[] getAll<T>()
        {
            using (BinaryReader dReader = new BinaryReader(dataStream), hReader = new BinaryReader(headStream))
            {
                if (hReader.BaseStream.Length == 0)
                {
                    return default(T[]);
                }
                hReader.BaseStream.Position = 0;
                int n = hReader.ReadInt32();
                int k = hReader.ReadInt32();

                T[] array = new T[n];

                if (n == 0)
                {
                    return default(T[]);
                }

                int index = 0;
                for (int i = 0; i < k; i++)
                {
                    long posh = 8 + (4 * i);
                    hReader.BaseStream.Position = posh;

                    int id = hReader.ReadInt32();

                    if(id == 0)             //NUEVO 
                    {
                        continue;
                    }

                    long posd = SIZE * (id - 1);
                    dReader.BaseStream.Position = posd;

                    T newValue = (T)Activator.CreateInstance(typeof(T));
                    PropertyInfo[] info = newValue.GetType().GetProperties();

                    foreach (PropertyInfo pinfo in info)
                    {
                        Type type = pinfo.PropertyType;

                        if (type == typeof(int))
                        {
                            pinfo.SetValue(newValue, dReader.ReadInt32()); //ERROR
                        }
                        else if (type == typeof(long))
                        {
                            pinfo.SetValue(newValue, dReader.ReadInt64());
                        }
                        else if (type == typeof(float))
                        {
                            pinfo.SetValue(newValue, dReader.ReadSingle());
                        }
                        else if (type == typeof(double))
                        {
                            pinfo.SetValue(newValue, dReader.ReadDouble());
                        }
                        else if (type == typeof(decimal))
                        {
                            pinfo.SetValue(newValue, dReader.ReadDecimal());
                        }
                        else if (type == typeof(char))
                        {
                            pinfo.SetValue(newValue, dReader.ReadChar());
                        }
                        else if (type == typeof(string))
                        {
                            pinfo.SetValue(newValue, dReader.ReadString());
                        }
                        else if (type == typeof(bool))
                        {
                            pinfo.SetValue(newValue, dReader.ReadBoolean());
                        }
                    }
                    array[index] = newValue;
                    index++;
                }
                return array;
            }

        }

        public bool removeById(int id)
        {
            using (BinaryReader hreader = new BinaryReader(headStream), dreader = new BinaryReader(dataStream))
            {
                using (BinaryWriter hwriter = new BinaryWriter(hreader.BaseStream), dwriter = new BinaryWriter(dreader.BaseStream))
                {
                    hreader.BaseStream.Position = 0;
                    int n = hreader.ReadInt32();
                    int k = hreader.ReadInt32();

                    if (id < 0 || id > k || id == 0)
                    {
                        return false;
                    }

                    for (int i = 0; i < k; i++)
                    {
                        long poshW;
                        //long poshR;

                        if (id - 1 == i)
                        {
                            poshW = 8 + (i * 4);
                            hwriter.BaseStream.Position = poshW;
                            hwriter.Write(0);
                            hwriter.BaseStream.Position = 0;
                            hwriter.Write(--n);
                            return true;
                        }
                        //if (id - 1 == 0)
                        //{
                        //    poshW = 8 + (4 * (i - 1));
                        //    poshR = 8 + (4 * i);
                        //    hwriter.BaseStream.Position = poshW;
                        //    hreader.BaseStream.Position = poshR;
                        //    hwriter.Write(hreader.ReadInt32());

                        //    //poniendo el actual en 0
                        //    hwriter.BaseStream.Position = poshR;
                        //    hwriter.Write(0);
                        //}
                        //else
                        //{
                        //    poshW = 8 + (4 * i);
                        //    poshR = 8 + (4 * i);
                        //    hwriter.Write(hreader.ReadInt32());

                        //}
                    }
                    return false;

                }
            }
        }

        public bool updateById<T>(T t, int id)
        {
            T objeto = getById<T>(id);

            if(objeto != null)
            {
                using(BinaryWriter dWriter = new BinaryWriter(dataStream))
                {
                    long pos = SIZE * (id - 1);
                    dWriter.BaseStream.Position = pos;

                    PropertyInfo[] info = t.GetType().GetProperties();
                    foreach (PropertyInfo pinfo in info)
                    {
                        Type type = pinfo.PropertyType;
                        Object value = pinfo.GetValue(t);

                        if (type.IsGenericType)
                        {
                            continue;
                        }
                        if (pinfo.Name.Equals("Id", StringComparison.CurrentCultureIgnoreCase))
                        {
                            dWriter.Write(id);
                            continue;
                        }
                        if (type == typeof(int))
                        {
                            dWriter.Write((int)value);
                        }
                        else if (type == typeof(long))
                        {
                            dWriter.Write((long)value);
                        }
                        else if (type == typeof(float))
                        {
                            dWriter.Write((float)value);
                        }
                        else if (type == typeof(double))
                        {
                            dWriter.Write((double)value);
                        }
                        else if (type == typeof(decimal))
                        {
                            dWriter.Write((decimal)value);
                        }
                        else if (type == typeof(string))
                        {
                            dWriter.Write((string)value);
                        }
                        else if (type == typeof(char))
                        {
                            dWriter.Write((char)value);
                        }
                        else if (type == typeof(bool))
                        {
                            dWriter.Write((bool)value);
                        }
                    }
                }
            }

            return false;
        }
    }
}
