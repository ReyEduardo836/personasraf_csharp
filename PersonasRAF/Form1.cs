﻿using PersonasRAF.IO;
using PersonasRAF.poco;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonasRAF
{
    public partial class Form1 : Form
    {
        RafContent raf;
        PersonasControl personasControl;
        bool editMode = false;
        int idUpdate;
        public Form1()
        {
            InitializeComponent();
            raf = new RafContent("Personas", 500);
            dgvPersonas.DataSource = raf.getAll<Persona>();
            MostrarPersonas();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text == "" || txtApellido.Text == "" || txtPais.Text == "" || txtTelefono.Text == "" || txtImagen.Text == "")
            {
                MessageBox.Show("Rellene todos los campos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!editMode)
            {
                string nombre = txtNombre.Text;
                string apellido = txtApellido.Text;
                string telefono = txtTelefono.Text;
                string pais = txtPais.Text;

                raf.create<Persona>(new Persona()
                {
                    Nombre = nombre,
                    Apellido = apellido,
                    Telefono = telefono,
                    Pais = pais,
                    URL = txtImagen.Text
                });
            }
            else if (editMode)
            {
                string nombre = txtNombre.Text;
                string apellido = txtApellido.Text;
                string telefono = txtTelefono.Text;
                string pais = txtPais.Text;

                raf.updateById<Persona>(new Persona()
                {
                    Nombre = nombre,
                    Apellido = apellido,
                    Telefono = telefono,
                    Pais = pais,
                    URL = txtImagen.Text
                }, idUpdate);
                editMode = false;
            }

            dgvPersonas.DataSource = raf.getAll<Persona>();
            MostrarPersonas();

            txtNombre.Text = "";
            txtApellido.Text = "";
            txtPais.Text = "";
            txtTelefono.Text = "";
            txtImagen.Text = "";

        }

        //private void btnBuscar_Click(object sender, EventArgs e)
        //{
        //    int id = int.Parse(txtId.Text);
        //    Persona per = raf.getById<Persona>(id);

        //    Persona[] p = new Persona[1];
        //    p[0] = per;
        //    if (per == null)
        //    {
        //        dgvPersonas.DataSource = null;
        //        MessageBox.Show("El ID introducido no existe", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //        return;
        //    }
        //    dgvPersonas.DataSource = p;
        //}

        private void btnMostrar_Click(object sender, EventArgs e)
        {
            txtId.Text = "";
            dgvPersonas.DataSource = raf.getAll<Persona>();
            MostrarPersonas();
        }

        private void btnURL_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtImagen.Text = openFileDialog1.FileName;
            }
        }

        private void MostrarPersonas()
        {
            if (raf.getAll<Persona>() == null)
            {
                pnlMostrarPersonas.Controls.Clear();
                return;
            }

            pnlMostrarPersonas.Controls.Clear();
            foreach (Persona p in raf.getAll<Persona>())
            {
                personasControl = new PersonasControl(p.Nombre, p.Apellido, p.Telefono, p.Pais, p.URL);
                pnlMostrarPersonas.Controls.Add(personasControl);
            }

        }

        private void MostrarListaPersonas(List<Persona> lista)
        {
            if (lista == null) return;
            pnlMostrarPersonas.Controls.Clear();
            foreach (Persona per in lista)
            {
                personasControl = new PersonasControl(per.Nombre, per.Apellido, per.Telefono, per.Pais, per.URL);
                pnlMostrarPersonas.Controls.Add(personasControl);
            }
        }

        private void txtIDPress(object sender, KeyPressEventArgs e)
        {
            string idStr = "";
            int id = 0;
            if (Char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar) || char.IsWhiteSpace(e.KeyChar))
            {
                if (char.IsControl(e.KeyChar) || char.IsWhiteSpace(e.KeyChar))
                {
                    if (txtId.Text.Length == 0)
                    {
                        return;
                    }
                    idStr = txtId.Text.Substring(0, txtId.Text.Length - 1);
                }
                else
                {
                    idStr = txtId.Text + e.KeyChar;
                }
                if (idStr.Length != 0)
                {
                    id = int.Parse(idStr);
                }
                else if (idStr.Length == 0)
                {
                    id = 0;
                }

                Persona per = raf.getById<Persona>(id);

                List<Persona> p = new List<Persona>();

                p.Add(per);
                if (per == null)
                {
                    dgvPersonas.DataSource = null;
                    return;
                }
                dgvPersonas.DataSource = p;
                MostrarListaPersonas(p);
            }
            else
            {
                MessageBox.Show("Ingrese un dato valido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Handled = true;
                txtId.Text = "";
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (raf.getAll<Persona>() == null)
            {
                return;
            }
            List<Persona> per = new List<Persona>();

            string filtro = "";

            if (char.IsControl(e.KeyChar) || char.IsWhiteSpace(e.KeyChar))
            {
                if (txtFiltro.Text.Length == 0)
                {
                    return;
                }
                filtro = txtFiltro.Text.Substring(0, txtFiltro.Text.Length - 1);
            }
            else
            {
                filtro = txtFiltro.Text + e.KeyChar;
            }

            foreach (Persona p in raf.getAll<Persona>())
            {
                if (p.Nombre.Contains(filtro))
                {
                    per.Add(p);
                }
                else if (p.Apellido.Contains(filtro))
                {
                    per.Add(p);
                }
                else if (p.Telefono.Contains(filtro))
                {
                    per.Add(p);
                }
                else if (p.Pais.Contains(filtro))
                {
                    per.Add(p);
                }
                else if (p.URL.Contains(filtro))
                {
                    per.Add(p);
                }
            }
            dgvPersonas.DataSource = per;
            MostrarListaPersonas(per);
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            txtNombre.Text = "";
            txtApellido.Text = "";
            txtTelefono.Text = "";
            txtPais.Text = "";
            txtImagen.Text = "";
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvPersonas.DataSource == null)
            {
                return;
            }

            Persona p = (Persona)dgvPersonas.CurrentRow.DataBoundItem; // Asi se trae un objeto de una fila seleccionada
            if (p == null) return;
            bool flag = raf.removeById(p.Id);
            if (flag == true)
            {
                MessageBox.Show("El usuario se ha eliminado correctamente", "Eliminado Exitosamente", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("No se ha podido eliminar el usuario, intente nuevamente", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            dgvPersonas.DataSource = raf.getAll<Persona>();
            MostrarPersonas();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dgvPersonas.DataSource == null)
            {
                return;
            }
            editMode = true;
            Persona p = (Persona)dgvPersonas.CurrentRow.DataBoundItem;

            txtNombre.Text = p.Nombre;
            txtApellido.Text = p.Apellido;
            txtTelefono.Text = p.Telefono;
            txtPais.Text = p.Pais;
            txtImagen.Text = p.URL;

            idUpdate = p.Id;
        }
    }
}




