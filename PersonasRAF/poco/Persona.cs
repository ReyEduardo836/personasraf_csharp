﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PersonasRAF.poco
{
    class Persona
    {
        public int Id { get; set; }                     //4
        public string Nombre { get; set; }              //30 * 2 + 3 = 63
        public string Apellido { get; set; }            //30 * 2 + 3 = 63
        public string Telefono { get; set; }            //10 * 2 + 3 = 23
        public string Pais { get; set; }                //10 * 2 + 3 = 23
        public string URL { get; set; }                 // 150 * 2 + 3 = 303 
                                                        //TOTAL = 500
    }
}
