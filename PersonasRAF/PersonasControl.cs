﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PersonasRAF
{
    public partial class PersonasControl : UserControl
    {
        public PersonasControl(string Nombres, string Apellidos, string Telefono, string Pais, string URL)
        {
            InitializeComponent();
            lblNombres.Text = Nombres;
            lblApellidos.Text = Apellidos;
            lblTelefono.Text = Telefono;
            lblPais.Text = Pais;
            bxImagen.ImageLocation = URL;
        }
    }
}
